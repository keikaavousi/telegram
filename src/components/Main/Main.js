import React from "react";
import ChatForm from "../ChatForm/ChatForm";
import styles from "./Main.module.css";
import ChatBox from "../ChatBox/ChatBox";

const Main = (props) => {
  return (
    <main className={styles.main}>
      <ChatBox />
      <ChatForm />
    </main>
  );
};
export default Main;
