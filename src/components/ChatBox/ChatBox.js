import React, { useContext, useState } from "react";
import { ChatContext } from "../ChatContext/ChatContext";
import ChatBlob from "./ChatBlob";
import styles from "./ChatBox.module.css";
const ChatBox = (props) => {
  const [chat, setChat] = useContext(ChatContext);
  return (
    <div className={styles.chatbox}>
      {chat.map((msg, index) => {
        return <ChatBlob key={index} msg={msg.value} type={msg.type}/>;
      })}
    </div>
  );
};
export default ChatBox;
