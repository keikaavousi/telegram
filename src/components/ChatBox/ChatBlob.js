import React from "react";
import styles from './ChatBox.module.css'
const ChatBlob = (props) => {
  return <div className={[styles.box,props.type==='send'?styles.send:styles.receive].join(' ')}>{props.msg}</div>;
};
export default ChatBlob;
