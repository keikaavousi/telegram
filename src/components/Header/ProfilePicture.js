import React, { useState } from "react";
import styles from "./Header.module.css";
import Profile from "../Profile/Profile";
const ProfilePicture = (props) => {
  const [show, setShow] = useState(false);
  return (
    <>
      <img
        src={props.src}
        className={styles.img}
        alt="profile"
        onClick={() => setShow(true)}
      />
      <Profile
        show={show}
        img={props.src}
        lastseen={new Date().toLocaleDateString("en-US")}
        name="MohammadReza"
        mobile="09129310690"
        username="@keikaavousi"
        onClick={()=>setShow(false)}
      />
    </>
  );
};

export default ProfilePicture;
