import React from "react";
import styles from "./Header.module.css";
import ProfilePicture from "./ProfilePicture";

const Header = (props) => {
  return (
    <header className={styles.header}>
      <ProfilePicture src="/mrk.png" />
    </header>
  );
};
export default Header;
