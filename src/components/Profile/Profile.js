import React, { useState } from "react";
import styles from "./Profile.module.css";

const Profile = (props) => {
  return (
    <div
      className={[styles.modal, props.show ? styles.show : styles.hide].join(
        " "
      )}
    >
      <img src="/back.png" className={styles.back} onClick={props.onClick} />
      <img src={props.img} className={styles.img} />
      <div className={styles.info}>
        <h3>{props.name}</h3>
        <p>Last seen at {props.lastseen}</p>
        <label>Mobile: {props.mobile}</label>
        <label>Username: {props.username}</label>
      </div>
    </div>
  );
};
export default Profile;
