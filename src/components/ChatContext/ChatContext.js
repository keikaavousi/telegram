import React, { useState } from "react";

export const ChatContext = React.createContext();

export const ChatProvider = (props) => {
  const [chats, setChats] = useState([]);
  return (
    <ChatContext.Provider value={[chats, setChats]}>
      {props.children}
    </ChatContext.Provider>
  );
};
