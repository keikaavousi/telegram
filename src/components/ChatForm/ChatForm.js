import React, { useContext, useState } from "react";
import styles from "./ChatForm.module.css";
import { ChatContext } from "../ChatContext/ChatContext";

const ChatForm = (props) => {
  const [chat, setChat] = useContext(ChatContext);
  const [msg, setMsg] = useState({ type: "send", value: "" });
  const handleMsg = (e) => {
    setMsg({ ...msg,value: e.target.value });
  };
  const handleChat = (e) => {
    e.preventDefault();
    if (msg.value) {
      setChat((chat) => [...chat, msg]);
      setMsg({...msg, value: "" });
      setTimeout(() => {
        setChat((chat) => [...chat, { type: "receive", value: "Hi!!" }]);
      }, 2000);
    }
  };

  return (
    <form onSubmit={handleChat} className={styles.formbox}>
      <input
        className={styles.input}
        placeholder="Message"
        onChange={handleMsg}
        value={msg.value}
      />
      <button className={styles.button}>
        <img src="/send.png" alt="send button" />
      </button>
    </form>
  );
};
export default ChatForm;
